import 'package:flutter/material.dart';

import 'headerless_table.dart';
import 'simple_table.dart';
import 'custom_size_table.dart';
import 'themed_table.dart';

void main() {
  runApp(MaterialApp(
    title: 'Example',
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Examples")),
      body: Center(
        child: Column(
          children: <Widget>[
            RaisedButton(
              child: Text("Simple table"),
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SimpleTable())),
            ),
            RaisedButton(
              child: Text("Header-less table"),
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HeaderlessTable())),
            ),
            RaisedButton(
              child: Text("Custom column and row sizes table"),
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CustomSizeTable())),
            ),
            RaisedButton(
              child: Text("Themed table"),
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ThemedTable())),
            ),
          ],
        ),
      ),
    );
  }
}
